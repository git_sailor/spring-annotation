package com.annotation.config;

import com.annotation.bean.Person;
import com.annotation.service.BookService;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * 配置类 == 配置文件
 *
 * @author sailor wang
 * @date 2019/4/21 10:02 PM
 * @description
 */
@Configuration
//1.包含注解
//@ComponentScan(value = "com.annotation", includeFilters = {
//        @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {Controller.class, Service.class, Repository.class})
//}, useDefaultFilters = false)
//2.排除注解
//@ComponentScan(value = "com.annotation", excludeFilters = {
//        @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {Controller.class, Service.class, Repository.class})//要排除的注解
//}, useDefaultFilters = false)
//3.组合形式
@ComponentScans(
        value = {
                @ComponentScan(value = "com.annotation", excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {Controller.class, Service.class, Repository.class})
                })
        }
)
public class MainConfig {

    /**
     * 给容器注册一个bean；类型为返回值类型，id默认是方法名，也可以通过bean指定
     *
     * @return
     */
    @Bean("person")
    public Person person1() {
        return new Person("lisi", 20);
    }
}