package com.annotation;

import com.annotation.bean.Person;
import com.annotation.config.MainConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author sailor wang
 * @date 2019/4/21 9:54 PM
 * @description
 */
public class MainTest {
    public static void main(String[] args) {
        //xml配置方式
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans.xml");
        Person person = (Person) applicationContext.getBean("person");
        System.out.println(person);

        //bean注解方式
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(MainConfig.class);
        Person person1 = (Person) annotationConfigApplicationContext.getBean("person");
        System.out.println(person1);

        String[] beanDefinitionNames = annotationConfigApplicationContext.getBeanDefinitionNames();
        for (String definitionName : beanDefinitionNames) {
            System.out.println(definitionName);
        }
    }
}