# spring-annotation

#### 介绍
Spring注解驱动开发，分三个模块来学习：
+ 容器
    - AnnotationConfigApplicationContext
    - 组件添加
    - 组件赋值
    - 组件注入
    - AOP
    - 声明式事务
+ 扩展原理
    - BeanFactoryPostProcessor
    - BeanDefinitionRegistryPostProcessor
    - ApplicationListener
    - Spring容器创建过程
+ web
    - servlet3.0
    - 异步请求

